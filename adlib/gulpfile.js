'use strict';

var gulp = require('gulp');

// load plugins
var $ = require('gulp-load-plugins')();


gulp.task('styles', function () {
    return gulp.src('app/styles/**/main.styl')
        .pipe($.stylus() )
        .pipe(gulp.dest('.tmp/styles'));

});

gulp.task('scripts', function () {
    return gulp.src('app/scripts/**/*.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe(gulp.dest('.tmp/scripts'))
        .pipe($.size())
});

gulp.task('images', function () {
    return;

    // return gulp.src('app/images/**/*')
    //     .pipe($.imagemin({
    //         optimizationLevel: 3,
    //         progressive: true,
    //         interlaced: true,
    //     }))
    //     .pipe(gulp.dest('dist/images'))
    //     .pipe($.size());
});
//''templates','styles', 'scripts'
gulp.task('compile', ['templates','styles','scripts'], function () {
   
  
    var assets = $.useref.assets({searchPath: ['.tmp','app'] });
    var gulpif = require('gulp-if');
    var minifyHtml = require('gulp-minify-html');

    return gulp.src('.tmp/*.html')
        .pipe( assets )
        .pipe( gulpif( '*.js', $.uglify() ))
        .pipe( gulpif( '*.css', $.csso() ))
        .pipe( assets.restore() )
        .pipe( $.useref() )
        //.pipe( minifyHtml( {empty:true} ) )
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});




gulp.task('clean', function(cb) {
    var del = require('del');
    del.sync(['.tmp','dist']);
    cb();
});


gulp.task('connect', function () {
    var serveStatic = require('serve-static');
    var connect = require('connect')
    var app = connect();


    $.connect.server({
        root: [ 'app','.tmp' ],
        port:9000,
        livereload: true,

    });
});


gulp.task('open', ['connect', 'styles'], function () {
    require('opn')('http://localhost:9000');
});

gulp.task('templates', function(){
    return gulp.src( 'app/*.jade')
        .pipe( $.jade({pretty: true}) )
        .pipe( gulp.dest('.tmp') )
        .pipe( $.size() );

});

gulp.task('copy', function () {
    return gulp.src( [ 'app/fonts/**/*', 'app/downloads/**/*' ],{ base: 'app/' } )
        .pipe( gulp.dest('dist') )
        .pipe( $.size() );
})


// inject bower components
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;

    gulp.src('app/*.jade')
        .pipe(wiredep({
            directory: 'app/bower_components'
        }))
        .pipe(gulp.dest('app'));
});


gulp.task('build', ['clean','compile','images','copy']);

gulp.task('default', ['watch','open'] );

gulp.task('watch', ['templates','styles','scripts','connect' ], function () {
   
    gulp.watch([
        '.tmp/*.html',
        '.tmp/styles/**/*.css',
        '.tmp/scripts/**/*.js',
        'app/images/**/*'
    ]).on('change', function (file) {
        gulp.src( file.path)
            .pipe( $.connect.reload() );
    });

    
    gulp.watch('app/**/*.jade', ['templates']);
    gulp.watch('app/styles/**/*.styl', ['styles']);
    gulp.watch('app/scripts/**/*.js', ['scripts']);
    gulp.watch('app/images/**/*', ['images']);
    gulp.watch('bower.json', ['wiredep']);
    //gulp.watch('bower.json', ['wiredep']);
});

