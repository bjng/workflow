(function(){
	'use strict'




	// _requestAnimationFrame= ( function(){
	// 	return  window.requestAnimationFrame       ||
	// 		window.webkitRequestAnimationFrame ||
	// 		window.mozRequestAnimationFrame    ||
	// 		function( callback ){
	// 			window.setTimeout(callback, 1000 / 60);
	// 		};
	// }());

	var timeCurrent, timeLast = Date.now();
	var fpsDesired = 60; // your desired FPS, also works as a max
	var fpsAverage = fpsDesired;

	var fpsText = document.createElement( 'p' );
	document.querySelector('body').appendChild( fpsText );
	function fpsUpdate() {
    	fpsText.innerHTML = fpsAverage.toFixed(2);
	}



	var currentFrame = -1;
	var animationFrame =  (function(){
		return window.requestAnimationFrame       ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			function( callback ){
				window.setTimeout(callback, 1000 / 60);
			};
	}());

	var listeners = [];
	function listen( obj ){
		if(! obj.onFrame ){ 
			throw new Error( 'Object needs to have onFrame function implemented') 
		}
		listeners.push( obj );
	}

	var onFrame = function(){
		timeCurrent = Date.now();
		var fpsThisFrame = 1000 / (timeCurrent - timeLast);
		if(timeCurrent > timeLast) {
	        fpsAverage += (fpsThisFrame - fpsAverage) / 1;
	        timeLast = timeCurrent;
	    }

		currentFrame++;
		for(var i=0;i<listeners.length; i++){
			var delay = listeners[ i ].delay || 0;
			if(delay > currentFrame ){ continue }

			listeners[ i ].onFrame( currentFrame - delay );
		}

		animationFrame( onFrame );
		


	
		
	}



	setInterval(fpsUpdate,3000);
	function BackgroundStrategy(){};

	BackgroundStrategy.prototype = {
		init: function( view,image ){
			view.style.backgroundImage = 'url(' + image.src + ')';
			view.style.backgroundRepeat= 'no-repeat';
			view.style.display= 'block';
		},
		render: function( view, image,width,height,x,y ){
			view.style.backgroundPosition = x + 'px ' +  y + 'px';
		}
	}

	function CanvasStrategy(){
		this.canvas = document.createElement('canvas');
		this.ctx=this.canvas.getContext("2d");
	}

	CanvasStrategy.prototype = {
		init: function( view,image ){
			console.log('init c');
			this.canvas.width = parseFloat( view.style.width.replace('px','') );
			this.canvas.height = parseFloat( view.style.height.replace('px','') );
			view.appendChild( this.canvas );
		},
		render: function( view,image,width,height,x,y ){
			this.ctx.drawImage(image,x,y);
		}
	}




	animationFrame( onFrame );

	function SpriteAnimation( view ){
	
		var self = this;
		this.view = view;

		var strategy = this.view.getAttribute('data-sprite-strategy') || 'canvas';

		this.renderer = strategy == 'canvas' ? new CanvasStrategy() : new BackgroundStrategy();

		this.isHorizontal = this.view.getAttribute('data-sprite-horizontal') || false;
		this.loop = this.view.getAttribute('data-sprite-loop') || true;
		this.fps = parseInt(this.view.getAttribute('data-sprite-fps')) || 24;
		this.delay = parseInt(this.view.getAttribute('data-sprite-delay')) || 0;

		
		var size = ( this.view.getAttribute('data-sprite-size') || '100x100' ).split( 'x' );
		this.originalWidth = size[0];
		this.originalHeight = size[1];

		this.localFrame = 0;

		this.url = this.view.getAttribute('data-sprite');
		this.preload( this.url, function( img ){ 
			self.image = img;

			self.totalFrames = ( self.isHorizontal ? img.naturalWidth / self.originalWidth : img.naturalHeight / self.originalHeight ) | 0;
			self.init() 
			console.log( self.totalFrames/18 );
			if(self.onLoaded){
				self.onLoaded()
			}
		} );
	}

	SpriteAnimation.prototype= {
		init: function(){
			var self = this;
			this.view.style.width = this.originalWidth + 'px';
			this.view.style.height = this.originalHeight + 'px';
			this.renderer.init( this.view,this.image )
			listen( this );
		},
		onFrame: function( frame ){
			
			var i =  ( frame / ( 60 /  this.fps ) ) | 0;

				i = this.loop ? i%this.totalFrames
					: i >= this.totalFrames ? this.totalFrames-1 
					: i;

				
			var pos = { top:0, left:0 };
			pos.top = -i * this.originalHeight;

			this.renderer.render( this.view, this.image, this.originalWidth, this.originalHeight, pos.left, pos.top );



		},
		preload: function( url, callback ){
			var img = new Image();
		
			img.src=url;

			if(document.readyState === 'complete'){
				callback( img );
			}
			else{
				img.onload = function(){ callback( img ) };
			}


		}
	}

	


// onFrame(){ seek( frame ) };
// whichImage(){ 
// //some math
// 	return 200
// }



// seek( s ){
// 	var image = whichImage( s );

// }
// play()
// restart(){ seek(0),play()}
// stop(){
	
// }
	window.SpriteAnimation = SpriteAnimation;
}());