(function(){

	function Sticky( view ){
			this.view = $(view);
			this.parent = this.view.parent();
			this.init();
			
	}

	Sticky.prototype = {
		init: function(){
			var self = this;
			$(window).resize( function(){
				self.onResize();
			});

			$(window).scroll( function(){
				self.onScroll();
			});


		},
		onResize: function(){ this.update(); },
		onScroll: function(){ this.update(); },
		update: function(){ 

			var offset = this.parent.offset().top,
				h = this.view.outerHeight(),
				max = this.parent.height() - h,
				scrollY = $(window).scrollTop() - offset;

			var top = this.view.position().top + h ;


			var onEdge = scrollY > offset + top
			
			if(onEdge){
				this.view.css('position', 'absolute');
				var pos = Math.max( 0, Math.min( max, scrollY - offset + h ) );
				this.view.css( 'margin-top', pos - top - 1 );
				this.view.addClass( 'sticky' );
				this.view.next( ).css('margin-top',h);

			}
			else{
				
				this.view.css('position', '');
				this.view.css( 'margin-top', '' );
				this.view.removeClass( 'sticky' );
				this.view.next( ).css('margin-top','');
			}

		

			
		}
	}
	window['Sticky'] = Sticky;

}());
