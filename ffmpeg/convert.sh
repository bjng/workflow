
#!/bin/bash

shopt -s nocaseglob nullglob

for f in ./input/*.{mov,mp4,webm,flv,avi,mpeg4}
do
	ffmpeg -i "${f}" -c:v libx264 -preset slow -profile:v baseline -movflags faststart -y  ./output/$(basename "${f%.*}").web.mp4
	ffmpeg -i "${f}" -y ./output/$(basename "${f%.*}").web.webm
done


