
#!/bin/bash

shopt -s nocaseglob nullglob

for f in ./input/*.{mov,mp4,webm,flv,avi,mpeg4}
do
  ffmpeg -i "${f}" -r 18 -an -f image2 ./output/$(basename "${f%.*}")_%05d.png
done
§