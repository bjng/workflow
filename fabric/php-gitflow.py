from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm


git = 'git@bitbucket.org:twentyonetwelve/PROJECT_GIT'
project_name = "PROJECT-NAME"

def dev():
	env.name = 'development'
	env.hosts=['root@IP']
	env.branch = 'develop'
	env.root = 	'/var/www/%s' % project_name

'''
def prod():
	env.name = 'production'
	env.hosts=['root@134.213.53.161']
	env.branch = 'master'
	env.root = 	'/var/www/%s' % project_name
'''


def setup():
	if len(env.hosts):
		pass

	with settings(warn_only=True):
		if run("test -d %s" % env.root).failed:
			run("mkdir %s" % env.root)
		
		run("git clone %s %s" % (git, env.root))

		with cd(env.root):
			run("git checkout %s" % env.branch)

		
def deploy():
	with settings(warn_only=True):
		with cd(env.root):
			run("git checkout %s" % env.branch)
			run("git reset --hard")
			run("git pull origin %s" % env.branch)

def keygen():
	if not env.host:
		print('Specify host first')
		return
	run( 'ssh-keygen -t rsa -C "%s.%s"' % (env.name,project_name) )

def key():
	if not env.host:
		print('Specify host first')
		return
	run('cat ~/.ssh/id_rsa.pub')

def connect():
	if not env.host:
		print('Specify host first')
		return
	local('ssh %s' % env.hosts[0] )

def authorize():
	if not env.host:
		print('Specify host first')
		return
	
	local("cat ~/.ssh/id_rsa.pub | ssh %s 'cat >> .ssh/authorized_keys'" % env.hosts[0])

''' 
GitFlow helpers for main develop branch
flow - created new develop branch if it doesn't exists and also sets the upstream to origin/develop
push:'message' - commits my changes and pushes them to develop branch
merge - merges develop changes with master, push the new stuff to orgin/master and return to develop
'''

def flow():
	with settings(warn_only=True):
		local('git branch develop')
		local('git branch --set-upstream develop origin/develop')

	local('git checkout develop')

def push(message):
	with settings(warn_only=True):
		local("git commit -a -m '%s'" %message)
		local("git push origin develop")


def merge():
	local('git checkout master')
	local('git pull origin master')
	local('git merge develop --commit')
	local('git push origin master')
	local('git checkout develop')


