from fabric.api import *


project_name  = 'economist-citi-progressmakersatwork'
git = 'git@bitbucket.org:twentyonetwelve/economist-citi-benchmark.git'

def dev():
	env.name = 'development' 
	env.hosts = [ 'root@162.13.86.94' ]
	env.branch = 'develop'
	env.root = '/var/www/economist-citi-progressmakersatwork'
	

def prod():
	env.name = 'production'
	env.hosts = [ 'root@198.61.189.15' ]
	env.branch = 'master'
	env.root = '/var/www/economist-citi-progressmakersatwork'
	

	#env.hosts = [ 'YOURHOST' ]



def setup():
	if len(env.hosts):
		pass

	with settings(warn_only=True):
		run("npm install pm2@0.9.1 -g --unsafe-perm")

		if run("test -d %s" % env.root).failed:
			run("git clone %s %s" % (git, env.root))

		with cd(env.root):
			run("git checkout %s" % env.branch)
			run("npm install")	

def deploy():
	with settings(warn_only=True):
		with cd(env.root):
			run("git checkout %s" % env.branch)
			run("git reset --hard")
			run("git pull origin %s" % env.branch)
			run("npm install")

def start():
	with settings(warn_only=True):
		with cd(env.root):
			run( "NODE_ENV=%s pm2 start bin/www --name %s" % (env.name,project_name) )

def restart():
	with settings(warn_only=True):
		with cd(env.root):
			run( "NODE_ENV=%s pm2 restart %s" % (env.name,project_name) )

def status():
	with cd(env.root):
		run("pm2 list")


''' 
Server authorisation helpers
'''


def keygen():
	if not env.host:
		print('Specify host first')
		return
	run( 'ssh-keygen -t rsa -C "%s.%s"' % (env.name,project_name) )

def key():
	if not env.host:
		print('Specify host first')
		return
	run('cat ~/.ssh/id_rsa.pub')

def connect():
	if not env.host:
		print('Specify host first')
		return
	local('ssh %s' % env.hosts[0] )


def authorize():
	if not env.host:
		print('Specify host first')
		return
	
	local("cat ~/.ssh/id_rsa.pub | ssh %s 'cat >> .ssh/authorized_keys'" % env.hosts[0])



''' 
GitFlow helpers for main develop branch
flow - created new develop branch if it doesn't exists and also sets the upstream to origin/develop
push:'message' - commits my changes and pushes them to develop branch
merge - merges develop changes with master, push the new stuff to orgin/master and return to develop
'''

def flow():
	with settings(warn_only=True):
		local('git branch develop')
		local('git branch --track develop origin/develop')

	local('git checkout develop')

def push(message):
	with settings(warn_only=True):
		local("git commit -a -m '%s'" %message)
		local("git push origin develop")


def merge():
	local('git checkout master')
	local('git pull origin master')
	local('git merge develop --commit')
	local('git push origin master')
	local('git checkout develop')

